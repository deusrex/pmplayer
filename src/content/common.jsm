var EXPORTED_SYMBOLS = ["Cc","Ci","Cu","PMPlayer","PMPrefListener"]
const {classes: Cc, interfaces: Ci, utils: Cu} = Components;
Cu.import("resource://gre/modules/Services.jsm");
if ("undefined" == typeof(PMPlayer)) {
	var PMPlayer = {};
}
PMPlayer.Common = {
		logoutput:false,
		setLogging:function(flag){
			PMPlayer.Common.logoutput=flag;
		},
		/**
		 * Log output to console if enabled.
		 */ 
		printlog:function(item) {
			if(PMPlayer.Common.logoutput==true) {
				Services.console.logStringMessage("[PMPlayer:]"+item);
			}
		},
		sites:new Set(['\.youtube\.com\/','\.youtu\.be\/','\.dailymotion\.com\/','\.vimeo\.com\/','\.bitchute\.com\/',
			'\/videos\/watch/','(\.(aac|gifv|mov|mp4|oga|ogv|webm))$','\.substack.com\/[0-9]','nicovideo.jp']),
			globals: {
				PMPcommand : null,
				browser	   : null,
				PMPcontext : null,
				validPage  : false,
				screensize : 25,
				mnuTools : null,
				validPageListenerStub : function(flag){},
				frameEmbed : null,
				setValidPage : function(flag){
					this.validPage=flag;
					this.validPageListenerStub(flag);
				},
				registerListener: function(listener) {
					this.validPageListenerStub = listener;
				}
			},
			validPageListener:function(enable){
				if(!enable || Services.wm.getMostRecentWindow("pmplayer:window")!=null){
					PMPlayer.Common.globals.PMPcommand.setAttribute("disabled","true");
					PMPlayer.Common.globals.PMPcommand.setAttribute("tooltiptext",PMPlayer.Launch.txtDisabled);
				}
				else {
					PMPlayer.Common.globals.PMPcommand.removeAttribute("disabled");
					PMPlayer.Common.globals.PMPcommand.setAttribute("tooltiptext",PMPlayer.Launch.txtEnabled);
				}
			},
			URLBuilder :{
				extractEmbed:function(aURI){
					var embedURL;
					var regex;
					var videoID = null;
					if(aURI.host.toLowerCase().indexOf("youtu")>-1){
						embedURL = "https://www.youtube.com/embed/VIDEOID?enablejsapi=1&amp;modestbranding=1&amp;fs=0";
						regex = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
						videoID = aURI.spec.match(regex) != null? aURI.spec.match(regex)[1]:null;
						PMPlayer.Common.printlog("Youtube video, URI="+aURI.spec+", video ID = "+videoID);
					} else if(aURI.host.toLowerCase().indexOf("dailymotion")>-1){
						embedURL = "https://www.dailymotion.com/embed/video/VIDEOID";
						regex=/^.+dailymotion.com\/(video|hub)\/([^_\?]+)[^#]*(#video=([^_&]+))?/i;
						let result = aURI.spec.match(regex);
						if(result!=null){
							if(result[4]!=undefined){
								videoID=result[4];
							} else {
								videoID=result[2];
							}
						}
						PMPlayer.Common.printlog("Dailymotion video, URI= "+aURI.spec+", video ID = "+videoID);
					} else if(aURI.host.toLowerCase().indexOf('vimeo')>-1){
						embedURL="https://player.vimeo.com/video/VIDEOID";
						regex=/https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/i;
						videoID = aURI.spec.match(regex)!=null?aURI.spec.match(regex)[3]:null;
						PMPlayer.Common.printlog("Vimeo video, URI= "+aURI.spec+", video ID = "+videoID);
					} else if(aURI.host.toLowerCase().indexOf('bitchute')>-1){
						embedURL="https://www.bitchute.com/embed/VIDEOID";
						regex=/https?:\/\/(?:www\.|)?bitchute.com\/video\/(.+)/i;
						videoID=aURI.spec.match(regex)!=null?aURI.spec.match(regex)[1]:null;
						PMPlayer.Common.printlog("Bitchute video, URI= "+aURI.spec+", video ID = "+videoID);
					} else if(aURI.spec.toLowerCase().indexOf('/videos/watch/')>-1){ // peertube
						// instance
						regex=/videos\/watch\/(.+)/i;
						PMPlayer.Common.printlog("Peertube instance, URI= "+aURI.spec);
						return aURI.spec.match(regex)==null?null:aURI.spec.replace("watch","embed");
					} else if(aURI.spec.indexOf('?')==-1){
						PMPlayer.Common.printlog("extractEmbed: media link found, URI="+aURI.spec);
						return aURI.spec;
					} else if(aURI.host.toLowerCase().indexOf('nicovideo.jp')>-1) {
						embedURL=" https://embed.nicovideo.jp/watch/VIDEOID";
						regex=/sm[0-9]{8}/gi;
						videoID=aURI.spec.match(regex)?aURI.spec.match(regex)[0]:null;
					}
					return videoID == null ? null : embedURL.replace('VIDEOID',videoID);;
				},
				generateEmbed:function(aURI,fromPage){
					PMPlayer.Common.printlog("aURI is "+aURI.spec);
					if(aURI.scheme.startsWith("http")||aURI.spec.startsWith("blob")){
						for(let site of PMPlayer.Common.sites){
							let re=new RegExp(site);
							if(re.test(aURI.spec)){ //  supported video URL
								PMPlayer.Common.globals.frameEmbed = PMPlayer.Common.URLBuilder.extractEmbed(aURI);
								PMPlayer.Common.printlog("generateEmbed-matched link: "
								                         +aURI.spec+"\n frame URI= "
								                         +PMPlayer.Common.globals.frameEmbed);
								if(PMPlayer.Common.globals.frameEmbed!=null){
									PMPlayer.Common.globals.setValidPage(fromPage);
									return true;
								}
							} else {
								PMPlayer.Common.globals.setValidPage(false);
							}
						}
					} else {
						PMPlayer.Common.globals.setValidPage(false);
					}
					return false;
				}
			},
			installButton:function(doc,toolbarId,id,afterId){
					if (!doc.getElementById(id)) {
						var toolbar = doc.getElementById(toolbarId);
						PMPlayer.Common.printlog("toolbar ID is "+toolbarId+" toolbar is "+toolbar);
						var before = null;
						if (afterId) {
							let elem = doc.getElementById(afterId);
							if (elem && elem.parentNode == toolbar){
								before = elem.nextElementSibling;
							}
						}
						toolbar.insertItem(id,before);
						toolbar.setAttribute("currentset", toolbar.currentSet);
						PMPlayer.Common.printlog("current set is "+toolbar.currentSet);
						doc.persist(toolbar.id, "currentset");
						if (toolbarId == "addon-bar"){
							toolbar.collapsed = false;
						}
					}
			},
			prefHandler : new PMPrefListener("extensions.PMPlayer.",function(branch,name){
				switch(name){
					case("logging"):
						PMPlayer.Common.setLogging(Services.prefs.getBoolPref("extensions.PMPlayer.logging",false));
						break;
					case("toolsmenu"):
						PMPlayer.Common.printlog("hidden tools pref changed");
						PMPlayer.Common.globals.mnuTools.setAttribute('hidden',!Services.prefs.getBoolPref("extensions.PMPlayer.toolsmenu",true));
						break;
				}
			}),
}
function PMPrefListener(branch_name, callback) {
	  // Keeping a reference to the observed preference branch or it will get
	  // garbage collected.
	  this._branch = Services.prefs.getBranch(branch_name);
	  this._branch.QueryInterface(Ci.nsIPrefBranch2);
	  this._callback = callback;
	}

	PMPrefListener.prototype.observe = function(subject, topic, data) {
	  if (topic == 'nsPref:changed')
	    this._callback(this._branch, data);
	};

	PMPrefListener.prototype.register = function() {
	  this._branch.addObserver('', this, false);
	    let that = this;
	    this._branch.getChildList('', {}).
	      forEach(function (pref_leaf_name){ 
	    	  that._callback(that._branch, pref_leaf_name); 
	      });
	};

	PMPrefListener.prototype.unregister = function() {
	  if (this._branch)
	    this._branch.removeObserver('', this);
	};	