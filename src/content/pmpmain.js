Components.utils.import("chrome://pmplayer/content/common.jsm");
Cu.import("resource://gre/modules/XPCOMUtils.jsm");
Cu.import("resource://gre/modules/Services.jsm");
if ("undefined" == typeof (PMPlayer)) {
  var PMPlayer = {};
}
PMPlayer.Launch = {
  mnuContext: null,
  contextMenu: null,
  strBundle: null,
  txtEnabled: null,
  txtDisabled: null,
  init: function() {
    var firstRun = Services.prefs.getBoolPref("extensions.PMPlayer.firstrun", true);
    if (firstRun) {
      Services.prefs.setBoolPref("extensions.PMPlayer.firstrun", false);
      Services.prefs.setBoolPref("extensions.PMPlayer.logging", false);
      PMPlayer.Common.installButton(document, "nav-bar", "button-pmplayer-play");
      PMPlayer.Common.installButton(document, "addon-bar", "button-pmplayer-play");
    } else {
      PMPlayer.Common.setLogging(Services.prefs.getBoolPref("extensions.PMPlayer.logging"));
    }
    PMPlayer.Common.globals.screensize = Services.prefs.getIntPref("extensions.PMPlayer.windowpercent", 25);
    PMPlayer.Common.globals.browser = gBrowser;
    PMPlayer.Common.printlog("screensize = " + PMPlayer.Common.globals.screensize);
    PMPlayer.Launch.mnuContext = document.getElementById("pmplayer-context");

    PMPlayer.Common.globals.mnuTools = document.getElementById("menu-pmplayer-play");
    PMPlayer.Common.globals.PMPcommand = document.getElementById("CmdSetPMPlayerOpenWindow");
    PMPlayer.Common.globals.PMPcontext = document.getElementById("CmdSetPMPlayerContextMenu");
    PMPlayer.Launch.contextMenu = document.getElementById('contentAreaContextMenu');

    let hide = Services.prefs.getBoolPref("extensions.PMPlayer.toolsmenuhidden", true);
    PMPlayer.Common.globals.mnuTools.setAttribute("hidden", hide ? "true" : "false");
    PMPlayer.Launch.contextMenu.addEventListener('popupshowing', PMPlayer.Launch.checkLink);
    PMPlayer.Common.globals.PMPcommand.addEventListener('command', PMPlayer.Launch.openWindowFromPage);
    PMPlayer.Common.globals.PMPcontext.addEventListener('command', PMPlayer.Launch.openWindow);
    PMPlayer.Launch.strBundle = document.getElementById("pmplayer-strings");
    PMPlayer.Launch.txtDisabled = PMPlayer.Launch.strBundle.getString("tooltipDisabled");
    PMPlayer.Launch.txtEnabled = PMPlayer.Launch.strBundle.getString("tooltipEnabled");
    PMPlayer.Common.globals.registerListener(PMPlayer.Common.validPageListener);
    PMPlayer.Common.prefHandler.register();
    gBrowser.addProgressListener(PMPlayer.Launch.tabSwitchListener);
    PMPlayer.Common.URLBuilder.generateEmbed(gBrowser.currentURI);
  },
  openWindowFromPage: function() {
    PMPlayer.Common.URLBuilder.generateEmbed(gBrowser.currentURI);
    PMPlayer.Launch.openWindow();
  },
  openWindow: function() {
    window.open("chrome://pmplayer/content/pmpwindow.xul",
      "pmplayerwindow", "chrome,width=" + window.screen.width * PMPlayer.Common.globals.screensize / 100 +
      ",height=" + window.screen.height * PMPlayer.Common.globals.screensize / 100 +
    ",resizable=yes,dialog,alwaysRaised,centerscreen");
  },
  checkLink: function(event) {
    try {
      var element = event.target.triggerNode;
      let embedded = false;
      let URI = null;
      let node=element.nodeName.toUpperCase();
      PMPlayer.Common.printlog("Current node= "+node);
      let videolist=element.querySelectorAll('video');
      PMPlayer.Common.printlog("video nodes present = "+videolist.length);
      if (node == "VIDEO") {
        URI = Services.io.newURI(element.getAttribute("src"), null, null);
        PMPlayer.Common.printlog("URI is "+URI.spec);
        embedded = PMPlayer.Common.URLBuilder.generateEmbed(URI, false);
      } else if (gContextMenu.inFrame && 
        element.ownerDocument.defaultView.frameElement.nodeName.toUpperCase() == "IFRAME" 
        && !gContextMenu.onLink) {
          embedded = true;
          PMPlayer.Common.globals.frameEmbed = element.ownerDocument.documentURI;
      } else {
        URI = gContextMenu.onLink ? gContextMenu.linkURI : gBrowser.currentURI;
        embedded = PMPlayer.Common.URLBuilder.generateEmbed(URI, false);
      }
      gContextMenu.showItem('pmplayer-context', embedded && Services.wm.getMostRecentWindow("pmplayer:window") == null);
    } catch (e) {
      Cu.reportError("[PMPlayer error:] " + e);
    }
  },
  tabSwitchListener: {
    QueryInterface: XPCOMUtils.generateQI(["nsIWebProgressListener", "nsISupportsWeakReference"]),
    onStateChange: function(aWebProgress, aRequest, aFlag, aStatus) {
    },
    onLocationChange: function(aProgress, aRequest, aURI) {
      PMPlayer.Common.printlog("on location change, URL= " + aURI.spec);
      PMPlayer.Common.URLBuilder.generateEmbed(aURI, true);
    }
  },
  cleanup: function() {
    gBrowser.removeProgressListener(PMPlayer.Launch.tabSwitchListener);
    PMPlayer.Common.globals.PMPcommand.removeEventListener('command', PMPlayer.Launch.openWindowFromPage);
    PMPlayer.Common.globals.PMPcontext.removeEventListener('command', PMPlayer.Launch.openWindow);
    PMPlayer.Launch.contextMenu.removeEventListener('popupshowing', PMPlayer.Launch.checkLink);
    PMPlayer.Common.prefHandler.unregister();
    var lastWindow = Services.wm.getMostRecentWindow("navigator:browser");
    if (lastWindow != null) { //Switch UI references to next open window. 
      PMPlayer.Common.printlog("Closing this window, link of next one is " + lastWindow.gBrowser.currentURI.spec);
      PMPlayer.Common.globals.mnuTools = lastWindow.document.getElementById("menu-pmplayer-play");
      PMPlayer.Common.globals.PMPcommand = lastWindow.document.getElementById("CmdSetPMPlayerOpenWindow");
      PMPlayer.Common.globals.PMPcontext = lastWindow.document.getElementById("CmdSetPMPlayerContextMenu");
      PMPlayer.Launch.contextMenu = lastWindow.document.getElementById('contentAreaContextMenu');
    }
  }
}
window.addEventListener("load", PMPlayer.Launch.init, false);
window.addEventListener("unload", PMPlayer.Launch.cleanup, false);