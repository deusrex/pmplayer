Components.utils.import("chrome://pmplayer/content/common.jsm");
Cu.import("resource://gre/modules/Services.jsm");
if ("undefined" == typeof(PMPlayer)) {
	var PMPlayer = {};
}
PMPlayer.Options = {
		chkToolsMenu:null,
		txtScreenSize:null,
		prefTools:null,
		prefScreenSize:null,
		intScreenSize:25,
		init:function(){
			PMPlayer.Options.chkToolsMenu=document.getElementById("chk-toolsmenu");			
			PMPlayer.Options.txtScreenSize=document.getElementById("txt-screensize");
			PMPlayer.Options.prefTools=document.getElementById('pmpref-toolsmenu');
			PMPlayer.Options.prefScreenSize=document.getElementById("pmpref-screensize");
			PMPlayer.Options.chkToolsMenu.setAttribute("onsynctopreference","PMPlayer.Options.applyToolsPreference();");
			PMPlayer.Options.txtScreenSize.setAttribute("onsynctopreference"," return PMPlayer.Options.syncToScreenSizePreference();");
			PMPlayer.Options.txtScreenSize.value=Services.prefs.getIntPref("extensions.PMPlayer.windowpercent",25);
			document.getElementById("pmplayer-options").setAttribute("ondialogaccept","return PMPlayer.Options.onOk();");
		},
		applyToolsPreference:function(){
			var hide = PMPlayer.Options.prefTools.value;
			PMPlayer.Common.globals.mnuTools.setAttribute('hidden',hide ? 'false':'true');
		},
		syncToScreenSizePreference:function(){
			var pref = PMPlayer.Options.txtScreenSize.value;
			PMPlayer.Common.globals.screensize=pref;
			let pmplayerwindowref=Services.wm.getMostRecentWindow("pmplayer:window")
			if(pmplayerwindowref!=null){
				pmplayerwindowref.resizeTo(window.screen.width*PMPlayer.Common.globals.screensize/100,window.screen.height*PMPlayer.Common.globals.screensize/100);
			}
			return pref;
		},
		onOk:function(){
			PMPlayer.Common.printlog("on Ok");
		}
}