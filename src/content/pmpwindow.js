Components.utils.import("chrome://pmplayer/content/common.jsm");
Cu.import("resource://gre/modules/Services.jsm");
if ("undefined" == typeof(PMPlayer)) {
	var PMPlayer = {};
}
PMPlayer.PlayerWindow = {
		embed:null,
		init:function(){
			PMPlayer.Common.globals.PMPcommand.setAttribute("disabled","true");
			PMPlayer.PlayerWindow.init.embed=document.getElementById("pmplayer-browser");
			PMPlayer.PlayerWindow.init.embed.loadURI(PMPlayer.Common.globals.frameEmbed,null,null);
		},
		cleanup:function(){
			PMPlayer.Common.globals.frameEmbed=null;
				var lastWindow=Services.wm.getMostRecentWindow("navigator:browser");
				if(lastWindow!=null){
					PMPlayer.Common.printlog("Found last window "+lastWindow.gBrowser.currentURI.spec);
				}
				PMPlayer.Common.globals.frameEmbed = null;
				PMPlayer.Common.URLBuilder.generateEmbed(lastWindow.gBrowser.currentURI,true);
		}
}
window.addEventListener("load", PMPlayer.PlayerWindow.init,false);
window.addEventListener("unload", PMPlayer.PlayerWindow.cleanup,false);