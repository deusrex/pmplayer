[UXP](http://thereisonlyxul.org/) browser extension to play videos from supported sites in a separate popup window. 

Install for supported browsers: 
- [Pale Moon & Borealis](https://addons.palemoon.org/addon/pmplayer)

[Usage instructions](https://deusrex.neocities.org/xul/pmplayer/pmplayer.html)